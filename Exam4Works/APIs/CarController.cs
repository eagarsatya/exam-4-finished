using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Exam4Works.Models;
using Exam4Works.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using Minio;

namespace Exam4Works.APIs
{
    [Route("api/v1/car")]
    [ApiController]
    [Authorize]
    public class CarController : Controller
    {

        private readonly CarServices _CarServices;

        public CarController(CarServices carServices)
        {
            this._CarServices = carServices;
        }

        [HttpPost(Name = "insert-car")]
        public async Task<ActionResult> InsertCar([FromBody] CarViewModelForm model)
        {

            if (ModelState.IsValid == false)
            {
                return BadRequest("Model State is not valid");
            }

            var isSuccess = await this._CarServices.InsertCar(model);

            if (isSuccess == false)
            {
                return BadRequest("Failed to insert Car");
            }

            return Ok();
        }

        [HttpGet(Name = "get-all-car")]
        public async Task<ActionResult<List<CarViewModel>>> GetAllCar()
        {
            if (ModelState.IsValid == false)
            {
                return BadRequest("Model state is not valid");
            }

            var allCar = await this._CarServices.GetAllCar();

            return allCar;
        }

        [HttpGet("/get-brands2")]
        public ActionResult<List<String>> GetAllBrands2()
        {
            if (ModelState.IsValid == false)
            {
                return BadRequest();
            }

            var allBrands = this._CarServices.GetAllBrand2();

            return Ok(allBrands);
        }

        [HttpGet("/get-brands")]
        public async Task<ActionResult<List<string>>> GetAllBrands()
        {
            var allBrands = await this._CarServices.GetAllBrand();

            return Ok(allBrands);
        }

        [HttpGet("/get-types/{brand}")]
        public async Task<ActionResult<List<string>>> GetAllTypes(string brand)
        {
            if (ModelState.IsValid == false)
            {
                return BadRequest();
            }

            var allTypes = await this._CarServices.GetAllType(brand);

            return Ok(allTypes);
        }

        [HttpGet("get-a-car/{brand}/{type}", Name = "get-a-car")]
        public async Task<ActionResult> GetACar(string brand, string type)
        {
            if (ModelState.IsValid == false)
            {
                return BadRequest();
            }

            var getACar = await this._CarServices.GetACar(brand, type);

            if (getACar == null)
            {
                return BadRequest();
            }

            return Ok(getACar);
        }


        [HttpGet("{id}",Name = "get-image")]
        public async Task<ActionResult> GetImage(string id)
        {
            if (ModelState.IsValid == false)
            {
                return BadRequest();
            }

            var url = await this._CarServices.GetImageMiniO(id);

            return Redirect(url);
        }

        [HttpGet("/get-all-rent",Name = "get-all-rent")]
        public async Task<ActionResult<List<RentViewModel>>> GetAllRent()
        {

            var allRent = await this._CarServices.GetAllRent(User.Identity.Name);

            return allRent;
        }

        [HttpPost("/rent-a-car",Name = "rent-a-car")]
        public async Task<ActionResult> RentACar([FromBody] RentModel model)
        {
            if(ModelState.IsValid == false)
            {
                return BadRequest("Model state is not valid");
            }
            
            var userid = await this._CarServices.GetUserId(User.Identity.Name);

            model.UserId = userid;

            var isSuccess = await this._CarServices.RentACar(model);

            if(isSuccess == false)
            {
                return BadRequest("Failed to Rent A Car");
            }

            return Ok();
        }

        [HttpGet("get-a-rental/{id}", Name = "get-a-rental-by-id")]
        public async Task<ActionResult<RentModel>> GetARentalById(int id)
        {
            if (ModelState.IsValid == false)
            {
                return BadRequest();
            }

            var findRent = await this._CarServices.GetARentById(id);

            if (findRent == null)
            {
                return NotFound();
            }

            return findRent;
        }

        [HttpPost("update-rent",Name = "update-rent")]
        public async Task<ActionResult> UpdateRental([FromBody] RentModel model)
        {
            if(ModelState.IsValid == false)
            {
                return BadRequest("Model state is not valid");
            }

            var isSucces = await this._CarServices.UpdateRent(model);

            if(isSucces == false)
            {
                return BadRequest("Update failed");
            }

            return Ok();
        }

        [HttpDelete("delete-rent/{id}", Name = "delete-rent")]
        public async Task<ActionResult> DeleteRental(int id)
        {
            if(ModelState.IsValid == false)
            {
                return BadRequest("Model state is not valid");
            }

            var isSuccess = await this._CarServices.DeleteRent(id);

            if(isSuccess == false)
            {
                return BadRequest("Rent Id Not Found");
            }

            return Ok();
        }

    }
}