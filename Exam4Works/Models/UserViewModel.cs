using System.ComponentModel.DataAnnotations;

namespace Exam4Works.Models
{
    public class UserViewModel
    {
        public int UserId { get; set; }
        public string Username { get; set; }
        public string UserPassword { get; set; }
        public string Role { get; set; }
    }
    
    public class UserViewModelForm
    {
        public int UserId { get; set; }
        [Required]
        [StringLength(255)]
        public string Username { get; set; }
        [Required]
        [StringLength(255)]
        public string UserPassword { get; set; }
        public string Role { get; set; }
    }
    
}