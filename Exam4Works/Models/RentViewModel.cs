﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Exam4Works.Models
{
    public class RentViewModel
    {
        public int RentId { get; set; }
        public string Username { get; set; }
        public string VehicleRegistrationNumber { get; set; }
        public string Brand { get; set; }
        public string Type { get; set; }
        public DateTimeOffset RentTime { get; set; }
    }
}
