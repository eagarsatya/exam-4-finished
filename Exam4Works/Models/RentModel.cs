﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Exam4Works.Models
{
    public class RentModel
    {
        public int RentId { get; set; }
        public DateTimeOffset RentTime { get; set; }
        public int CarId { get; set; }
        public int UserId { get; set; }
    }
}
