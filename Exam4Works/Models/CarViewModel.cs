using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Http;

namespace Exam4Works.Models
{
    public class CarViewModel
    {
       
        public int CarId { get; set; }
        public string VehicleRegistrationNumber { get; set; }
        public string Brand { get; set; }
        public string Type { get; set; }
        public string Image { get; set; }

    }
    
    public class CarViewModelForm
    {

        public int CarId { get; set; }
        [Required]
        [Display(Name = "Vehicle Register Number")]
        [StringLength(50)]
        public string VehicleRegistrationNumber { get; set; }
        [Required]
        [StringLength(100)]
        public string Brand { get; set; }
        [Required]
        [StringLength(50)]
        public string Type { get; set; }
        public IFormFile Image { get; set; }

    }
}