﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Exam4Works.Models
{
    public class BrandViewModel
    {
        public List<string> Brand { get; set; }
    }
}
