import {Exam4} from "../rest-api/exam4";
import {CarViewModel, CarViewModelForm, RentModel, RentViewModel} from "../rest-api/models";
import { CONNREFUSED } from 'dns';

export class CarServices {

    cars: CarViewModel[] = [];
    brands: string[] = [];
    types: string[] = [];
    car: CarViewModel = {};
    rentals: RentViewModel[] = [];
    rental: RentModel = {};
    id: number;
    currentPage: number = 0;
    pageSize: number = 3;
    visibleRentals: RentViewModel[] = [];


    async getCars() {
        let api = new Exam4({
            baseUri: window.location.origin
        });

        let getAllCars = await api.getAllCar();
        let allCars = getAllCars._response.parsedBody;
        console.log("get cars run");

        console.log(allCars);

        this.cars = allCars;
        console.log("get cars run 2");
        console.log(this.cars);
    }

    async getBrands() {
        let api = new Exam4({
            baseUri: window.location.origin
        });

        let result = await api.getAllBrands();
        let allBrands = result._response.parsedBody;
        this.brands = allBrands;
        console.log("getBrand  : ", this.brands);
        //this.brands = result._response.parsedBody();
        //let allBrands = result._response.parsedBody;
    }

    async getTypes(brand: string) {
        let api = new Exam4({
            baseUri: window.location.origin
        });

        let result = await api.getAllTypes(brand);
        let allTypes = result._response.parsedBody;
        this.types = allTypes;
        console.log("get types : ", this.types);
    }

    async getACar(keyBrand:string,keyType:string) {
        let api = new Exam4({
            baseUri: window.location.origin
        });

        let result = await api.getACar(keyBrand,keyType);
        let aCar = result._response.parsedBody;
        this.car = aCar;
        console.log("get car : ", this.car);
    }

    async addRental(rentCar: RentModel) {
        let api = new Exam4({
            baseUri: window.location.origin
        });

        let result = await api.rentACar({model:rentCar});
    }

    async getAllRent() {
        let api = new Exam4({
            baseUri: window.location.origin
        });

        let result = await api.getAllRent();
        let allRent = result._response.parsedBody;
        this.rentals = allRent;
        console.log("allRent  : ", this.rentals);
    }

    async getARent(id:number) {
        console.log("getARent Running");
        let api = new Exam4({
            baseUri: window.location.origin
        });
        let result = await api.getARentalById(Number(id));
        let aRent = result._response.parsedBody;
        this.rental = aRent;
        console.log("getARent : ", this.rental);
    }

    async updateRent(updateCar: RentModel) {
        let api = new Exam4({
            baseUri: window.location.origin
        });

        let result = await api.updateRent({ model: updateCar });
    }

    async cancelRent(id: number) {
        console.log(id);
        let api = new Exam4({
            baseUri: window.location.origin
        });
        let result = await api.deleteRent(id);
    }

    updatePage(pageNumber: number) {
        this.currentPage = pageNumber;
        this.updateVisibleRental
    }

    updateVisibleRental() {
        this.visibleRentals = this.rentals.slice(this.currentPage * this.pageSize, (this.currentPage * this.pageSize) + this.pageSize);

        if (this.visibleRentals.length == 0 && this.currentPage > 0) {
            this.updatePage(this.currentPage - 1);
        }
    }



}

export let carServiceSingleton = new CarServices();