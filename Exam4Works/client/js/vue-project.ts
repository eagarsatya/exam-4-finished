import Vue from 'vue';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';
import VeeValidate from 'vee-validate';
import Hello from './components/Hello.vue';
import Display from "./components/Display.vue";
import InsertRental from "./components/InsertRental.vue";
import DisplayRental from './components/DisplayRental.vue';
import EditRental from './components/EditRental.vue';
import Datetime from 'vue-datetime';
import 'vue-datetime/dist/vue-datetime.css';
import DeleteRental from './components/DeleteRental.vue';
import Paginate from 'vuejs-paginate'

Vue.use(Datetime);

Vue.use(VeeValidate, {
    classes: true
});

Vue.component('fa', FontAwesomeIcon);

// components must be registered BEFORE the app root declaration
Vue.component('hello', Hello);
Vue.component('display', Display);
Vue.component('insert-rental', InsertRental);
Vue.component('display-rental', DisplayRental);
Vue.component('edit-rental', EditRental);
Vue.component('delete-rental', DeleteRental);
Vue.component('paginate', Paginate)

// bootstrap the Vue app from the root element <div id="app"></div>
new Vue().$mount('#app');
