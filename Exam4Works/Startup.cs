using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Entities.Exam4.PostGre;
using Exam4Works.Constants;
using Exam4Works.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Minio;

namespace Exam4Works
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<CookiePolicyOptions>(options =>
            {
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });

            services.AddAuthentication(UserAuthenticatedSchemes.Cookie).AddCookie(UserAuthenticatedSchemes.Cookie,options =>
            {
                options.LoginPath = "/Auth/Login";
                options.LogoutPath = "/Auth/Logout";
                options.AccessDeniedPath = "/Auth/Denied";

                options.Cookie.IsEssential = true;
                options.Cookie.HttpOnly = true;
                options.Cookie.SecurePolicy = CookieSecurePolicy.SameAsRequest;
            });
            
            services.AddSwaggerGen(config =>
            {
                config.SwaggerDoc("v1", new Swashbuckle.AspNetCore.Swagger.Info
                {
                    Title = "Exam4",
                    Version = "v1"
                });
            });
            
            services.AddDbContextPool<PostGreDbContext>(options =>
            {
                options.UseNpgsql(Configuration.GetConnectionString("PostGreConnString"),
                    strategy => { strategy.EnableRetryOnFailure(); });
            });

            services.AddScoped<UserServices>();
            services.AddScoped<CarServices>();

            services.AddSingleton<MinioClient>(di =>
            {
                var url = this.Configuration["MinIO:Url"];
                var accessKey = this.Configuration["MinIO:AccessKey"];
                var secretKey = this.Configuration["MinIO:SecretKey"];
                return new MinioClient(url, accessKey, secretKey);
            });


            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseAuthentication();
            
            
            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseCookiePolicy();

            app.UseSwagger();
            app.UseSwaggerUI(config =>
            {
                config.SwaggerEndpoint("/swagger/v1/swagger.json", "Exam4");
            });


            app.UseMvc();
        }
    }
}