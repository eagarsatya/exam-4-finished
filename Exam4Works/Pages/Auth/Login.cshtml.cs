using System;
using System.Threading.Tasks;
using Exam4Works.Constants;
using Exam4Works.Models;
using Exam4Works.Services;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace Exam4Works.Pages.Auth
{
    public class Login : PageModel
    {

        private readonly UserServices _UserServices;
        
        [BindProperty] public UserViewModelForm Form { get; set; }
        
        [TempData] public string Message { get; set; }
        
        public Login(UserServices userServices)
        {
            this._UserServices = userServices;
        }
        
        public void OnGet()
        {
            
        }

        public async Task<ActionResult> OnPostAsync(string url)
        {

            var loginUser = await this._UserServices.ValidateLogin(Form);

            if (loginUser == null)
            {
                Message = "Failed to login! Username or Password does not match";
                return Page();
            }

            var claims = this._UserServices.GenerateClaims(loginUser);
            
            var persistence = new AuthenticationProperties
            {
                ExpiresUtc = DateTimeOffset.Now.AddMinutes(30),
                IsPersistent = true
            };

            await HttpContext.SignInAsync(UserAuthenticatedSchemes.Cookie, claims, persistence);

            if (string.IsNullOrEmpty(url) == false)
            {
                return LocalRedirect(url);
            }
            
            return RedirectToPage("/Index");
        }
        
    }
}