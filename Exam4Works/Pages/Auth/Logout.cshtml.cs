using System.Threading.Tasks;
using Exam4Works.Constants;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace Exam4Works.Pages.Auth
{
    public class Logout : PageModel
    {
        public async Task<IActionResult> OnGet()
        {
            if (User.Identity.IsAuthenticated)
            {
                await HttpContext.SignOutAsync(UserAuthenticatedSchemes.Cookie);
            }
            return Redirect("~/Auth/Login");
        }
    }
}