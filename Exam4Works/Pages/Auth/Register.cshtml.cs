using System.Threading.Tasks;
using Exam4Works.Models;
using Exam4Works.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace Exam4Works.Pages.User
{
    public class Register : PageModel
    {

        private readonly UserServices _UserServices;
        
        [BindProperty]
        public UserViewModelForm Form { get; set; }

        public Register(UserServices userServices)
        {
            this._UserServices = userServices;
        }
        
        public void OnGet()
        {
            
        }

        public async Task<IActionResult> OnPost()
        {
            if (ModelState.IsValid == false)
            {
                return BadRequest("Model State is not valid");
            }

            var exist = await this._UserServices.CheckUserExist(Form.Username);

            if (exist == true)
            {
                return BadRequest("User already exist");
            }

            var isSuccess = await this._UserServices.AddUser(Form);

            if (isSuccess == false)
            {
                return BadRequest("Failed to register");
            }
            

            return RedirectToPage("/Index");
        }
        
    }
}