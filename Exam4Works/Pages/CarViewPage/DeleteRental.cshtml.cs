using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Exam4Works.Constants;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace Exam4Works.Pages.CarViewPage
{
    [Authorize(Roles = Roles.User + "," + Roles.Admin)]
    public class DeleteRentalModel : PageModel
    {
        [BindProperty(SupportsGet = true)]
        public int id { get; set; }
        public void OnGet()
        {
        }
    }
}