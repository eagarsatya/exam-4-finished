using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Exam4Works.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace Exam4Works.Pages.CarViewPage
{
    [Authorize]
    public class InsertRentalModel : PageModel
    {
        private readonly CarServices _CarServices;
        public InsertRentalModel(CarServices carServices)
        {
            this._CarServices = carServices;
        }

        public int userId { get; set; }

        public async Task OnGet()
        {
            var username = User.Identity.Name;

            userId = await this._CarServices.GetUserId(username);
        }
    }
}