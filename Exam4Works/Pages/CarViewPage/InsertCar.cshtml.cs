using System.Threading.Tasks;
using Exam4Works.Constants;
using Exam4Works.Models;
using Exam4Works.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace Exam4Works.Pages.CarViewPage
{
    [Authorize(Roles = Roles.Admin)]
    public class InsertCar : PageModel
    {

        private readonly CarServices _CarServices;
        
        [BindProperty] public CarViewModelForm Form { get; set; }

        public InsertCar(CarServices carServices)
        {
            this._CarServices = carServices;
        }
        
        public void OnGet()
        {
            
        }

        public async Task<ActionResult> OnPostAsync()
        {
            if (ModelState.IsValid == false)
            {
                return BadRequest("Model State is not valid");
            }

            var isSuccess = await this._CarServices.InsertCar(Form);

            if (isSuccess == false)
            {
                return BadRequest("Failed to insert car");
            }
            
            return RedirectToPage("/Index");
        }
        
    }
}