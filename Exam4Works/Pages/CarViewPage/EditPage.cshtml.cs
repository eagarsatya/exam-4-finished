using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace Exam4Works.Pages.CarViewPage
{
    [Authorize]
    public class EditPageModel : PageModel
    {
        [BindProperty(SupportsGet = true)]
        public int id { get; set; }
        public void OnGet()
        {
        }
    }
}