using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Entities.Exam4.PostGre;
using Exam4Works.Constants;
using Exam4Works.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Exam4Works.Services
{
    public class UserServices
    {
        private readonly PostGreDbContext _PostGreDbContext;

        public UserServices(PostGreDbContext postGreDbContext)
        {
            this._PostGreDbContext = postGreDbContext;
        }

        public async Task<bool> AddUser(UserViewModelForm model)
        {
            var password = BCrypt.Net.BCrypt.HashPassword(model.UserPassword, 12);

            var addUser = new TbUser()
            {
                Role = Roles.Admin,
                Username = model.Username,
                UserPassword = password
            };

            this._PostGreDbContext.TbUser.Add(addUser);
            await this._PostGreDbContext.SaveChangesAsync();

            return true;
        }

        public async Task<bool> CheckUserExist(string username)
        {
            var user = await this._PostGreDbContext.TbUser.Where(q => q.Username == username).FirstOrDefaultAsync();

            if (user != null)
            {
                return true;
            }
            
            return false;
        }

        public bool ValidatePassword(string password, string hashedPassword)
        {
            var isValid = BCrypt.Net.BCrypt.Verify(password, hashedPassword);

            if (isValid == false) return false;

            return true;
        }

        public async Task<UserViewModel> ValidateLogin(UserViewModelForm model)
        {
            var loginUser = await this._PostGreDbContext.TbUser.Where(q => q.Username == model.Username).Select(q =>
                new UserViewModel
                {
                    Role = q.Role,
                    Username = q.Username,
                    UserId = q.UserId,
                    UserPassword = q.UserPassword
                }).FirstOrDefaultAsync();

            if (loginUser != null)
            {
                if (ValidatePassword(model.UserPassword, loginUser.UserPassword) == false)
                {
                    return null;
                }
            }

            return loginUser;
        }

        public ClaimsPrincipal GenerateClaims(UserViewModel model)
        {
            var claims = new ClaimsIdentity(UserAuthenticatedSchemes.Cookie);

            claims.AddClaim(new Claim(ClaimTypes.NameIdentifier, model.Username));
            claims.AddClaim(new Claim(ClaimTypes.Name, model.Username));
            claims.AddClaim(new Claim(ClaimTypes.Role, model.Role));

            return new ClaimsPrincipal(claims);
        }
    }
}