using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Entities.Exam4.PostGre;
using Exam4Works.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Minio;

namespace Exam4Works.Services
{
    [Authorize]
    public class CarServices
    {

        private readonly PostGreDbContext _PostGreDbContext;

        private readonly MinioClient _Minio;

        public CarServices(PostGreDbContext postGreDbContext, MinioClient minio)
        {
            this._PostGreDbContext = postGreDbContext;
            this._Minio = minio;
        }

        public async Task<bool> InsertCar(CarViewModelForm model)
        {

            var imageBytes = new byte[0];
            var currentDate = DateTimeOffset.Now;

            using (var ms = new MemoryStream())
            {
                using (var stream = model.Image.OpenReadStream())
                {
                    await stream.CopyToAsync(ms);
                }

                imageBytes = ms.ToArray();
            }

            var blobId = Guid.NewGuid();
            var bucketName = "exam4";
            var location = "us-east-1";
            var objectName = blobId.ToString();
            var contentType = model.Image.ContentType;

            //Make bucket on MiniO Server, I guess :)

            bool found = await this._Minio.BucketExistsAsync(bucketName);
            if (found == false)
            {
                await this._Minio.MakeBucketAsync(bucketName, location);
            }

            using (var stream = model.Image.OpenReadStream())
            {
                await this._Minio.PutObjectAsync(bucketName, objectName, stream, stream.Length, contentType);
            }

            this._PostGreDbContext.Blob.Add(new Blob
            {
                ContentType = contentType,
                Name = objectName,
                BlobId = objectName,
            });

            this._PostGreDbContext.TbCar.Add(new TbCar
            {
                Type = model.Type,
                Brand = model.Brand,
                Image = objectName,
                VehicleRegistrationNumber = model.VehicleRegistrationNumber,
            });

            await this._PostGreDbContext.SaveChangesAsync();

            return true;
        }


        public async Task<ActionResult<List<CarViewModel>>> GetAllCar()
        {
            var carList = await this._PostGreDbContext.TbCar.Select(q => new CarViewModel
            {
                Type = q.Type,
                Brand = q.Brand,
                Image = q.Image,
                CarId = q.CarId,
                VehicleRegistrationNumber = q.VehicleRegistrationNumber
            }).ToListAsync();

            if (carList.Count == 0 || carList == null)
            {
                return new List<CarViewModel>();
            }
            return carList;
        }

        public async Task<string> GetImageMiniO(string id)
        {
            var bucketName = "exam4";
            var location = "us-east-1";
            var objectName = id;

            bool found = await this._Minio.BucketExistsAsync(bucketName);

            if (found == false)
            {
                await this._Minio.MakeBucketAsync(bucketName, location);
            }

            var expiry = (int)TimeSpan.FromMinutes(15).TotalSeconds;

            var url = await this._Minio.PresignedGetObjectAsync(bucketName, id, expiry);

            return url;
        }

        public async Task<bool> InsertRent(RentModel model)
        {
            this._PostGreDbContext.TbRent.Add(new TbRent
            {
                CarId = model.CarId,
                RentTime = model.RentTime,
                UserId = model.UserId
            });

            await this._PostGreDbContext.SaveChangesAsync();

            return true;
        }

        public async Task<BrandViewModel> GetBrand()
        {

            var getBrand = await this._PostGreDbContext.TbCar.Select(Q => Q.Brand).Distinct().ToListAsync();

            if (getBrand.Count == 0 || getBrand == null)
            {
                return new BrandViewModel();
            }

            BrandViewModel listBrand = new BrandViewModel();

            foreach (var brand in getBrand)
            {
                listBrand.Brand.Add(brand);
            }

            return listBrand;
        }

        public async Task<List<string>> GetAllBrand()
        {
            var getAll = await this._PostGreDbContext.TbCar.Select(Q => Q.Brand).Distinct().ToListAsync();

            return getAll;
        }

        public List<String> GetAllBrand2()
        {

            var getAll = this._PostGreDbContext.TbCar.Select(Q => Q.Brand).Distinct().ToList();

            return getAll;
        }

        public async Task<BrandViewModel> GetAllListBrand()
        {
            var getAll = await this._PostGreDbContext.TbCar.Select(Q => Q.Brand).Distinct().ToListAsync();

            BrandViewModel allBrand = new BrandViewModel();

            foreach (var item in getAll)
            {
                allBrand.Brand.Add(item);
            }

            return allBrand;
        }

        public async Task<List<string>> GetAllType(string brand)
        {
            var getAll = await this._PostGreDbContext.TbCar.Where(Q => Q.Brand == brand).Select(Q => Q.Type).Distinct().ToListAsync();

            return getAll;
        }
        public async Task<CarViewModel> GetACar(string brand, string type)
        {
            var getACar = await this._PostGreDbContext.TbCar.Where(Q => Q.Brand == brand && Q.Type == type).Select(Q => new CarViewModel
            {
                Brand = Q.Brand,
                CarId = Q.CarId,
                Type = Q.Type,
                Image = Q.Image,
                VehicleRegistrationNumber = Q.VehicleRegistrationNumber
            }).FirstOrDefaultAsync();

            return getACar;
        }

        public async Task<bool> RentACar(RentModel model)
        {

            this._PostGreDbContext.TbRent.Add(new TbRent
            {
                CarId = model.CarId,
                RentTime = DateTimeOffset.UtcNow,
                UserId = model.UserId
            });

            await this._PostGreDbContext.SaveChangesAsync();

            return true;
        }

        public async Task<int> GetUserId(string username)
        {
            var userId = await this._PostGreDbContext.TbUser.Where(Q => Q.Username == username).Select(Q => Q.UserId).FirstOrDefaultAsync();

            return userId;
        }

        public async Task<List<RentViewModel>> GetAllRent(string username)
        {

            var userId = await GetUserId(username);

            var query = from rent in _PostGreDbContext.TbRent
                        join user in _PostGreDbContext.TbUser on rent.UserId equals user.UserId
                        join car in _PostGreDbContext.TbCar on rent.CarId equals car.CarId
                        where user.UserId == userId
                        select new RentViewModel
                        {
                            Brand = car.Brand,
                            RentId = rent.RentId,
                            RentTime = rent.RentTime,
                            Type = car.Type,
                            Username = user.Username,
                            VehicleRegistrationNumber = car.VehicleRegistrationNumber
                        };

            var result = await query.ToListAsync();

            if (result == null || result.Count == 0)
            {
                new List<RentViewModel>();
            }

            return result;
        }

        public async Task<ActionResult<RentModel>> GetARentById(int id)
        {
            var findRent = await this._PostGreDbContext.TbRent.Select(Q => new RentModel
            {
                CarId = Q.CarId,
                RentId = Q.RentId,
                RentTime = Q.RentTime,
                UserId = Q.UserId
            }).Where(Q => Q.RentId == id).FirstOrDefaultAsync();

            if (findRent == null)
            {
                return null;
            }

            return findRent;
        }

        public async Task<bool> UpdateRent(RentModel model)
        {
            var updateRent = await this._PostGreDbContext.TbRent.FirstOrDefaultAsync(Q => Q.RentId == model.RentId);

            if(updateRent == null)
            {
                return false;
            }

            updateRent.RentTime = model.RentTime;
            updateRent.CarId = model.CarId;

            this._PostGreDbContext.TbRent.Update(updateRent);
            await this._PostGreDbContext.SaveChangesAsync();

            return true;
        }

        public async Task<bool> DeleteRent(int id)
        {
            var deleteRent = await this._PostGreDbContext.TbRent.FirstOrDefaultAsync(Q => Q.RentId == id);

            if (deleteRent == null)
            {
                return false;
            }

            this._PostGreDbContext.TbRent.Remove(deleteRent);
            await this._PostGreDbContext.SaveChangesAsync();
            return true;
        }

    }
}