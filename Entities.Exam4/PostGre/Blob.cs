﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Entities.Exam4.PostGre
{
    public partial class Blob
    {
        public Blob()
        {
            TbCar = new HashSet<TbCar>();
        }

        [StringLength(255)]
        public string BlobId { get; set; }
        [Required]
        [StringLength(255)]
        public string Name { get; set; }
        [Required]
        [StringLength(255)]
        public string ContentType { get; set; }

        [InverseProperty("ImageNavigation")]
        public virtual ICollection<TbCar> TbCar { get; set; }
    }
}
