﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Entities.Exam4.PostGre
{
    public partial class PostGreDbContext : DbContext
    {
        //public PostGreDbContext()
        //{
        //}

        public PostGreDbContext(DbContextOptions<PostGreDbContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Blob> Blob { get; set; }
        public virtual DbSet<TbCar> TbCar { get; set; }
        public virtual DbSet<TbRent> TbRent { get; set; }
        public virtual DbSet<TbUser> TbUser { get; set; }

//        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
//        {
//            if (!optionsBuilder.IsConfigured)
//            {
//#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
//                optionsBuilder.UseNpgsql("Server=localhost;Database=exam4;Username=postgres;Password=admin;");
//            }
//        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("ProductVersion", "2.2.4-servicing-10062");

            modelBuilder.Entity<Blob>(entity =>
            {
                entity.HasIndex(e => e.BlobId)
                    .HasName("BlobIdUnique")
                    .IsUnique();

                entity.Property(e => e.BlobId).ValueGeneratedNever();
            });

            modelBuilder.Entity<TbCar>(entity =>
            {
                entity.HasKey(e => e.CarId)
                    .HasName("PK_CarId");

                entity.HasIndex(e => e.VehicleRegistrationNumber)
                    .HasName("VehicleRegistrationNumberUnique")
                    .IsUnique();

                entity.Property(e => e.CarId).UseNpgsqlIdentityAlwaysColumn();

                entity.HasOne(d => d.ImageNavigation)
                    .WithMany(p => p.TbCar)
                    .HasForeignKey(d => d.Image)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("FK_Image");
            });

            modelBuilder.Entity<TbRent>(entity =>
            {
                entity.HasKey(e => e.RentId)
                    .HasName("PK_RentId");

                entity.Property(e => e.RentId).UseNpgsqlIdentityAlwaysColumn();

                entity.HasOne(d => d.Car)
                    .WithMany(p => p.TbRent)
                    .HasForeignKey(d => d.CarId)
                    .HasConstraintName("FK_CarId");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.TbRent)
                    .HasForeignKey(d => d.UserId)
                    .HasConstraintName("FK_UserId");
            });

            modelBuilder.Entity<TbUser>(entity =>
            {
                entity.HasKey(e => e.UserId)
                    .HasName("PK_UserId");

                entity.HasIndex(e => e.Username)
                    .HasName("UsernameUnique")
                    .IsUnique();

                entity.Property(e => e.UserId).UseNpgsqlIdentityAlwaysColumn();
            });
        }
    }
}
