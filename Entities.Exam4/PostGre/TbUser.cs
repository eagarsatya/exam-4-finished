﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Entities.Exam4.PostGre
{
    public partial class TbUser
    {
        public TbUser()
        {
            TbRent = new HashSet<TbRent>();
        }

        public int UserId { get; set; }
        [Required]
        [StringLength(255)]
        public string Username { get; set; }
        [Required]
        [StringLength(255)]
        public string UserPassword { get; set; }
        [Required]
        [StringLength(50)]
        public string Role { get; set; }

        [InverseProperty("User")]
        public virtual ICollection<TbRent> TbRent { get; set; }
    }
}
