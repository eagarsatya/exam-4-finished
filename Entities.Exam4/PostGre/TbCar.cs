﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Entities.Exam4.PostGre
{
    public partial class TbCar
    {
        public TbCar()
        {
            TbRent = new HashSet<TbRent>();
        }

        public int CarId { get; set; }
        [Required]
        [StringLength(50)]
        public string VehicleRegistrationNumber { get; set; }
        [Required]
        [StringLength(100)]
        public string Brand { get; set; }
        [Required]
        [StringLength(50)]
        public string Type { get; set; }
        [StringLength(255)]
        public string Image { get; set; }

        [ForeignKey("Image")]
        [InverseProperty("TbCar")]
        public virtual Blob ImageNavigation { get; set; }
        [InverseProperty("Car")]
        public virtual ICollection<TbRent> TbRent { get; set; }
    }
}
