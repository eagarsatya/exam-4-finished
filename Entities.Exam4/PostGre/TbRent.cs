﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Entities.Exam4.PostGre
{
    public partial class TbRent
    {
        public int RentId { get; set; }
        [Column(TypeName = "time with time zone")]
        public DateTimeOffset RentTime { get; set; }
        public int CarId { get; set; }
        public int UserId { get; set; }

        [ForeignKey("CarId")]
        [InverseProperty("TbRent")]
        public virtual TbCar Car { get; set; }
        [ForeignKey("UserId")]
        [InverseProperty("TbRent")]
        public virtual TbUser User { get; set; }
    }
}
