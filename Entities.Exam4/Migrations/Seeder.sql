--username : user
--password : username
INSERT INTO public."TbUser"(
	"Username", "UserPassword", "Role")
	VALUES ('user', '$2y$12$R61KBQ47k7h7wqbnRC4fB.M4bwn2IeajDpXEE/4M78wWxbJFfM.Lq', 'User');
	
--username : user2
--password : username

INSERT INTO public."TbUser"(
	"Username", "UserPassword", "Role")
	VALUES ('user2', '$2y$12$R61KBQ47k7h7wqbnRC4fB.M4bwn2IeajDpXEE/4M78wWxbJFfM.Lq', 'User');

--username : admin
--password : adminadmin
INSERT INTO public."TbUser"(
	"Username", "UserPassword", "Role")
	VALUES ('admin', '$2y$12$beYl6Yi6LwBu4D/5Cov4G.O0BHCPCd/PYKvXqlF6cHuHjhkoRm9vq', 'Admin');