--CREATE DATABASE Exam4

CREATE TABLE public."TbUser"
(
    "UserId" integer GENERATED ALWAYS AS IDENTITY NOT NULL,
    "Username" character varying(255) NOT NULL,
    "UserPassword" character varying(255) NOT NULL,
    "Role" character varying(50) NOT NULL,
    CONSTRAINT "PK_UserId" PRIMARY KEY ("UserId"),
    CONSTRAINT "UsernameUnique" UNIQUE ("Username")
        INCLUDE("Username")
)

CREATE TABLE public."Blob"
(
    "BlobId" CHARACTER VARYING(255) NOT NULL,
    "Name" CHARACTER VARYING(255) NOT NULL,
    "ContentType" CHARACTER VARYING(255) NOT NULL,

    CONSTRAINT "PK_BlobId" PRIMARY KEY ("BlobId"),
    CONSTRAINT "BlobIdUnique" UNIQUE ("BlobId") INCLUDE ("BlobId")
)

CREATE TABLE public."TbCar"
(
    "CarId" INTEGER GENERATED ALWAYS AS IDENTITY NOT NULL,
    "VehicleRegistrationNumber" CHARACTER VARYING(50) NOT NULL,
    "Brand" CHARACTER VARYING(100) NOT NULL,
    "Type" CHARACTER VARYING(50) NOT NULL,
    "Image" CHARACTER VARYING(255),

    CONSTRAINT "PK_CarId" PRIMARY KEY ("CarId"),
    CONSTRAINT "VehicleRegistrationNumberUnique" UNIQUE ("VehicleRegistrationNumber") INCLUDE ("VehicleRegistrationNumber"),
	
	CONSTRAINT "FK_Image" FOREIGN KEY ("Image") 
        REFERENCES public."Blob" ("BlobId") MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE CASCADE
)

CREATE TABLE public."TbRent"(
    "RentId" INTEGER GENERATED ALWAYS AS IDENTITY NOT NULL,
    "RentTime" TIMETZ NOT NULL,
    "CarId" INTEGER NOT NULL,
    "UserId" INTEGER NOT NULL,
    
    CONSTRAINT "PK_RentId" PRIMARY KEY ("RentId"),

    CONSTRAINT "FK_UserId" FOREIGN KEY ("UserId") 
        REFERENCES public."TbUser" ("UserId") MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE CASCADE,
    
    CONSTRAINT "FK_CarId" FOREIGN KEY ("CarId") 
        REFERENCES public."TbCar" ("CarId") MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE CASCADE
)

--Scaffold
--dotnet ef dbcontext scaffold "Server=localhost;Database=exam4;Username=postgres;Password=admin;" Npgsql.EntityFrameworkCore.PostgreSQL -o PostGre -c "PostGreDbContext" -f -d